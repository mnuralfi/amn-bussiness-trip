package amn.businesstrip.data.model

import android.content.ContentValues
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface AuthModel {
    @Headers(
        "Accept: application/json"
    )

    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<AuthResponse>

    @FormUrlEncoded
    @POST("register")
    fun register(
        @Field("name") name: String,
        @Field("address") address: String,
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<RegisterResponse>
}

data class AuthResponse (
    val status: Int,
    val values: List<ContenValue>
)

data class ContenValue(
    val name: String,
    val id: Int
)

data class MessageResponse(
    val status: Int,
    val values: String
)

data class RegisterResponse(
    val status: Int,
    val values: ContenValue
)