package amn.businesstrip.data.model

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface TripModel {
    @Headers(
        "Accept: application/json"
    )

    @FormUrlEncoded
    @POST("insert-trip")
    fun insertTrip(
        @Field("id_users") id_users: Int,
        @Field("visit_date") visit_date: String,
        @Field("destination") destination: String,
        @Field("total") total: Int
    ): Call<MessageResponse>
}

data class Rincian(
    val peruntukanRinci: String,
    val dateTimeRinci: String,
    val nominalRinci: Int,
    val photo: String
)