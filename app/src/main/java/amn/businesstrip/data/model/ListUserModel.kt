package amn.businesstrip.data.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Url

interface ListUserModel {
    @Headers(
        "Accept: application/json",
        "Content-type:application/json"
    )

    @GET("select")
    fun getListUser(
    ): Call<DataTrip>
}

data class DataTrip(
    val status: Int,
    val values: List<Trip>
)

data class Trip(
    val id: Int,
    val id_users: Int,
    val visit_date : String,
    val destination : String,
    val total: Int,
    val peruntukan: String,
    val tanggal: String,
    val jumlah: Int,
    val foto: String,
    val id_trip: Int
)