package amn.businesstrip.ui.home.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import amn.businesstrip.R
import amn.businesstrip.data.model.DataTrip
import amn.businesstrip.data.model.ListUserModel
import amn.businesstrip.services.ListUserAdapter
import amn.businesstrip.services.ServiceBuilder
import amn.businesstrip.ui.trip.Trip
import android.content.Intent
import kotlinx.android.synthetic.main.fragment_trip.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TripFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TripFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trip, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addNewTrip.setOnClickListener {
            val intent = Intent(context, Trip::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        fetchData()
    }

    fun fetchData(){
        val urlService = ServiceBuilder.buildService(ListUserModel::class.java)
        val requestCall = urlService.getListUser()
        requestCall.enqueue(object : Callback<DataTrip> {
            override fun onFailure(call: Call<DataTrip>, t: Throwable) {
            }
            override fun onResponse(
                call: Call<DataTrip>,
                response: Response<DataTrip>
            ) {
                if (response.isSuccessful) {

                    val users = response.body()?.values

                    if (users!!.isNotEmpty()) {
                        rvMember.visibility = View.VISIBLE
                        rvMember.adapter = ListUserAdapter(users)
                    }
                }
            }
        })

    }
}