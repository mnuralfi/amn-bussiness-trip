package amn.businesstrip.ui.home

import amn.businesstrip.R
import amn.businesstrip.ui.home.fragment.TripFragment
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView

class Home : AppCompatActivity() {
    lateinit var tripFragment : TripFragment
    lateinit var settingFragment : Home
    var lastSelected: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val bottomNavigation : BottomNavigationView = findViewById(R.id.btn_nav)

            tripFragment = TripFragment()
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame_container, tripFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()

        bottomNavigation.setOnNavigationItemSelectedListener OnNavigationItemSelectedListener@{ item ->

//            if (lastSelected != item.itemId){
//
//                bottomNavigation.menu.getItem(0).setIcon(R.drawable.home)
//                bottomNavigation.menu.getItem(1).setIcon(R.drawable.tab_activity3)
//                bottomNavigation.menu.getItem(2).setIcon(R.drawable.notif)
//                bottomNavigation.menu.getItem(3).setIcon(R.drawable.profile)

                when (item.itemId){

                    R.id.menu_home -> {
                        tripFragment = TripFragment()
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_container, tripFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                        lastSelected = item.itemId
                        return@OnNavigationItemSelectedListener true
                    }

                    R.id.menu_profil -> {
                        Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show()
//                        profileFragment =
//                            id.co.bnisyariah.employeeapp.Activity.Home.Fragment.ProfileFragment()
//                        supportFragmentManager
//                            .beginTransaction()
//                            .replace(R.id.frame_container, profileFragment)
//                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                            .commit()
//                        lastSelected = item.itemId
//                        return@OnNavigationItemSelectedListener true
                    }
                }
//            }
            false
        }
    }
}