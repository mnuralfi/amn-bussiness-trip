package amn.businesstrip.ui.register

import amn.businesstrip.R
import amn.businesstrip.data.model.AuthModel
import amn.businesstrip.data.model.RegisterResponse
import amn.businesstrip.services.ServiceBuilder
import amn.businesstrip.ui.login.LoginActivity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Register : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val actionBar = supportActionBar
        val backIcon = R.drawable.arrow_left
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(backIcon)
        actionBar?.title = "Register"

        btnRegister.setOnClickListener {
            val name = txtName.text.toString()
            val address = txtAddress.text.toString()
            val username = txtUsername.text.toString()
            val password = txtPassword.text.toString()

            if (name != null && address != null && username != null && password != null){
                register(name, address, username, password)
            }else {
                Toast.makeText(this, "Lengkapi Data", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun register(name: String, address: String, username: String, password: String ){
        val services = ServiceBuilder.buildService(AuthModel::class.java)
        val requestCall = services.register(name,address,username, password)
        requestCall.enqueue(object : Callback<RegisterResponse> {
            override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                Toast.makeText(applicationContext, "Error Occured" + t.localizedMessage, Toast.LENGTH_SHORT).show()
            }
            override fun onResponse(call: Call<RegisterResponse>, response: Response<RegisterResponse>) {
                if (response.isSuccessful) {
                    val intent = Intent(applicationContext, LoginActivity::class.java)
                    finish()
                    startActivity(intent)
                }else{
                    Toast.makeText(applicationContext, "Register Failed", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}