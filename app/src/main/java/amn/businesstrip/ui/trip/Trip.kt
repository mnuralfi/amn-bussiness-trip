package amn.businesstrip.ui.trip

import amn.businesstrip.R
import amn.businesstrip.data.model.MessageResponse
import amn.businesstrip.data.model.TripModel
import amn.businesstrip.services.ServiceBuilder
import amn.businesstrip.ui.login.LoginActivity
import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import id.co.bnisyariah.employeeapp.SharedPreference
import kotlinx.android.synthetic.main.activity_trip.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class Trip : AppCompatActivity() {
//    var formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var parser =  SimpleDateFormat("yyyy-MM-dd")
    val myFormat = "d-MMM-yyyy" //In which you need put here
    val myCalendar = Calendar.getInstance()
    val sdf = SimpleDateFormat(myFormat, Locale.US)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip)

        val session = SharedPreference(this)

        val actionBar = supportActionBar
        val backIcon = R.drawable.arrow_left
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(backIcon)
        actionBar?.title = "New Trip"
        addTrip.setOnClickListener {
            val intent = Intent(this, AddTrip::class.java)
            startActivity(intent)
        }

        val edittext = findViewById<TextView>(R.id.valDate)

       edittext.text = sdf.format(myCalendar.time)

        val name: String? = session.getValueString("name")
        val idUser: Int? = session.getValueInt("idUser")
        employee.text = name

        val destinations = valDestination.text.toString()


        fun updateLabel() {
            edittext.text = sdf.format(myCalendar.time)

            val visitDate = parser.format(myCalendar.time)
        }

        val date = object: DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year:Int, monthOfYear:Int,
                                   dayOfMonth:Int) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabel()
            }
        }

        calendar.setOnClickListener {
            DatePickerDialog(this@Trip, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    fun saveTrip(){

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.button_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val i = item.itemId
        if (i == R.id.save) {
//            Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show()
            val sharedPreference = SharedPreference(applicationContext)
            addTrip(sharedPreference.getValueInt("idUser"),parser.format(myCalendar.time),valDestination.text.toString())
        }
        return super.onOptionsItemSelected(item)
    }

    fun addTrip(id_user: Int, date: String, destination: String){
        val services = ServiceBuilder.buildService(TripModel::class.java)
        val requestCall = services.insertTrip(id_user,date,destination,0)
        requestCall.enqueue(object : Callback<MessageResponse> {
            override fun onFailure(call: Call<MessageResponse>, t: Throwable) {
                Toast.makeText(applicationContext, "Error Occured" + t.localizedMessage, Toast.LENGTH_SHORT).show()
            }
            override fun onResponse(call: Call<MessageResponse>, response: Response<MessageResponse>) {
                if (response.isSuccessful) {
                    onBackPressed();
                }else{
                    Toast.makeText(applicationContext, "Save Failed", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}