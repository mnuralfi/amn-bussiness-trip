package amn.businesstrip.ui.trip

import amn.businesstrip.R
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_trip.*
import java.text.SimpleDateFormat
import java.util.*

class DetailTrip : AppCompatActivity() {
    var DateFormatter = SimpleDateFormat("d-MMM-yyyy", Locale.US)
    var formatter = SimpleDateFormat("yyyy-MM-dd")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip)

        val actionBar = supportActionBar
        val backIcon = R.drawable.arrow_left
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(backIcon)
        actionBar?.title = "Trip Detail"

        addTrip.setOnClickListener {
            val intent = Intent(this, AddTrip::class.java)
            startActivity(intent)
        }

        val lemparan: Boolean? = intent.getBooleanExtra("throw", false)
        val idUser: Int? = intent.getIntExtra("id_users", 0)
        val name : String?= intent.getStringExtra("dtlName")
        val visitDate: String? = intent.getStringExtra("dtlVisitDate")
        val destination: String? = intent.getStringExtra("dtlDestination")
        val total: Int? = intent.getIntExtra("dtlTotal", 0)
        println(visitDate)
        if (lemparan!!){
            dataDetail.visibility  = View.VISIBLE
            rlDestination.visibility = View.GONE
            calendar.visibility = View.GONE
        }
        val date = formatter.parse(visitDate)
        employee.text = name
        valDate.text = DateFormatter.format(date)
        txtDestination.text = destination
        totalPengeluaran.text = total.toString()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}