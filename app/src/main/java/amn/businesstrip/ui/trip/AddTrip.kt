package amn.businesstrip.ui.trip

import amn.businesstrip.R
import amn.businesstrip.data.model.AuthModel
import amn.businesstrip.data.model.AuthResponse
import amn.businesstrip.data.model.Rincian
import amn.businesstrip.data.model.TripModel
import amn.businesstrip.services.ServiceBuilder
import amn.businesstrip.ui.login.LoginActivity
import android.Manifest
import android.Manifest.permission.CAMERA
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import id.co.bnisyariah.employeeapp.SharedPreference
import kotlinx.android.synthetic.main.activity_add_trip.*
import kotlinx.android.synthetic.main.activity_add_trip.calendar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_trip.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AddTrip : AppCompatActivity() {
    var parser =  SimpleDateFormat("yyyy-MM-dd")
    val myFormat = "d-MMM-yyyy" //In which you need put here
    val myCalendar = Calendar.getInstance()
    val sdf = SimpleDateFormat(myFormat, Locale.US)
    private val CAMERA = 2

    companion object {
        private val PERMISSION_CODE = 1001
    }

    var thumbnail: Bitmap? = null

    var listRincian: ArrayList<Rincian> = arrayListOf()
    var visitDate: String = ""
    var visitTime: String = ""

//    var valuePeruntukan: String = ""
//    var valueDateTime: String = ""
//    var valueNominal: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_trip)

        val actionBar = supportActionBar
        val backIcon = R.drawable.arrow_left
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeAsUpIndicator(backIcon)
        actionBar?.title = "Rincian"

        val peruntukan = valPeruntukan.text
        val dateTime = "$visitDate $visitTime"
        val nominal = valNominal.text

        val txtDate = findViewById<TextView>(R.id.valDateTime)

        txtDate.text = sdf.format(myCalendar.time)
        valTime.text = SimpleDateFormat("HH:mm").format(myCalendar.time)

        fun updateLabel() {
            txtDate.text = sdf.format(myCalendar.time)

            visitDate = parser.format(myCalendar.time)
        }

        val date = object: DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year:Int, monthOfYear:Int,
                                   dayOfMonth:Int) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabel()
            }
        }

        calendar.setOnClickListener {
            DatePickerDialog(this@AddTrip, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }


        time.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                valTime.text = SimpleDateFormat("HH:mm").format(cal.time)
                visitTime = SimpleDateFormat("HH:mm").format(cal.time)
            }
            TimePickerDialog(this, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
        }

        if (Build.VERSION.SDK_INT < 23){
            if (
                PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PermissionChecker.PERMISSION_DENIED ||
                PermissionChecker.checkSelfPermission(this, Manifest.permission.CAMERA) == PermissionChecker.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                ActivityCompat.requestPermissions(this, permissions,PERMISSION_CODE
                )
            }
        }else{
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions =
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                requestPermissions(permissions,PERMISSION_CODE
                )
            }
        }

        pictButton.setOnClickListener(View.OnClickListener { takePhotoFromCamera() })
        camera.setOnClickListener(View.OnClickListener { takePhotoFromCamera() })
//        Check.setOnClickListener {
////            listRincian.add(peruntukan)
////            listRincian.add(dateTime)
////            listRincian.add(nominal)
////            listRincian.add(thumbnail.toString())
//            println("=-=-=")
//            println(peruntukan)
//            println(dateTime)
//            println(nominal)
//            println("=-=-=")
//        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        thumbnail = data!!.extras!!.get("data") as Bitmap
        if (resultCode == Activity.RESULT_OK) {
            camera.visibility = View.VISIBLE
            camera!!.setImageBitmap(thumbnail)
            pictButton.visibility = View.GONE
        } else {
            camera.visibility = View.GONE
        }
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.button_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val i = item.itemId
        if (i == R.id.save) {
            Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show()
//            val intent = Intent()
////            intent.putExtra("member", memberList)
//            setResult(Activity.RESULT_OK, intent)
//            finish()

//            listRincian.add(peruntukan)
//            listRincian.add(dateTime)
//            listRincian.add(nominal)
//            listRincian.add(thumbnail.toString())
//
//            println(listRincian)
        }

        return super.onOptionsItemSelected(item)
    }
}
