package amn.businesstrip.ui.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast

import amn.businesstrip.R
import amn.businesstrip.data.model.AuthModel
import amn.businesstrip.data.model.AuthResponse
import amn.businesstrip.services.ServiceBuilder
import amn.businesstrip.ui.home.Home
import amn.businesstrip.ui.register.Register
import android.content.Intent
import id.co.bnisyariah.employeeapp.SharedPreference
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        val login = findViewById<Button>(R.id.login)
        
        actionBar?.hide()
        
        register.setOnClickListener {
            val intent = Intent(this, Register::class.java)
            startActivity(intent)
        }

            login.setOnClickListener {
                LoginClick()
            }
        }

    fun LoginClick() {
        val username = inputUsername.text.toString()
        val password = inputPassword.text.toString()

        val services = ServiceBuilder.buildService(AuthModel::class.java)
        val requestCall = services.login(username,password)
        requestCall.enqueue(object : Callback<AuthResponse> {
            override fun onFailure(call: Call<AuthResponse>, t: Throwable) {
                Toast.makeText(applicationContext, "Error Occured" + t.localizedMessage, Toast.LENGTH_SHORT).show()
            }
            override fun onResponse(call: Call<AuthResponse>, response: Response<AuthResponse>) {
                if (response.isSuccessful) {
                    val sharedPreference = SharedPreference(applicationContext)
                    val resp = response.body()
                    val data = response.body()?.values?.size

                    if (data != null && data != 0) {
                        resp?.values?.get(0)?.name?.let { sharedPreference.save("name", it) }
                        resp?.values?.get(0)?.id?.let { sharedPreference.save("idUser", it) }

                        val intent = Intent(applicationContext, Home::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    }else{
                        Toast.makeText(applicationContext, "Login Failed", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    Toast.makeText(applicationContext, "Login Failed", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
    }