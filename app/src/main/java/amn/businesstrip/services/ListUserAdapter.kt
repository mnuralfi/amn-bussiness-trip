package amn.businesstrip.services

import amn.businesstrip.R
import amn.businesstrip.data.model.Trip
import amn.businesstrip.ui.trip.DetailTrip
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.ramiz.nameinitialscircleimageview.MaterialColorGenerator
import com.github.ramiz.nameinitialscircleimageview.NameInitialsCircleImageView
import id.co.bnisyariah.employeeapp.SharedPreference
import java.text.SimpleDateFormat
import java.util.*

class ListUserAdapter(private val userList: List<Trip>) : RecyclerView.Adapter<ListUserAdapter.ViewHolder>() {
    var DateFormatter = SimpleDateFormat("d-MMM-yyyy", Locale.US)
    var formatter = SimpleDateFormat("yyyy-MM-dd")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListUserAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_member, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: ListUserAdapter.ViewHolder, position: Int) {
        val shared = SharedPreference(holder.itemView.context)
        val name = shared.getValueString("name")

        val date = formatter.parse(userList[position].visit_date)


        holder.trip = userList[position]
            holder.date.text = DateFormatter.format(date)
            holder.name.text = name
            holder.destination.text = userList[position].destination

            holder.itemView.setOnClickListener { v ->
                val context = v.context
                val intent = Intent(context, DetailTrip::class.java)
                intent.putExtra("id_users", userList[position].id_users)
                intent.putExtra("dtlName", name)
                intent.putExtra("dtlVisitDate", userList[position].visit_date)
                intent.putExtra("dtlDestination", userList[position].destination)
                intent.putExtra("dtlTotal", userList[position].total)
                intent.putExtra("throw", true)
//                intent.putExtra("todoEvent", userList[position].is_event)
//                intent.putExtra("todoContent", userList[position].content)
                context.startActivity(intent)

            }

            val nama = name?.trimEnd()!!.split(" ")
            val a: String
            a = if(nama.size > 1){
                nama[0].substring(0,1)
                println("_+_+_+_+_+")
                println(nama[0].substring(0,1)).toString()
            }else{
                nama.toString().substring(1,2).toUpperCase()
            }
                val imageInfo: NameInitialsCircleImageView.ImageInfo = NameInitialsCircleImageView.ImageInfo
                    .Builder(a)
                    .setColorGenerator(MaterialColorGenerator())
                    .build()
                holder.profile.setImageInfo(imageInfo)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val date: TextView = itemView.findViewById(R.id.date)
        val name: TextView = itemView.findViewById(R.id.nameMember)
        val destination: TextView = itemView.findViewById(R.id.destination)
        val profile : NameInitialsCircleImageView = itemView.findViewById(R.id.profileMember)
        var trip: Trip? = null

        override fun toString(): String {
            return """${super.toString()} '${name.text}' '${destination.text}' '${date.text}' '${profile }'"""
        }
    }

}