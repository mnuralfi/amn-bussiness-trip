package id.co.bnisyariah.employeeapp

import android.content.Context
import android.content.SharedPreferences
import android.widget.TextView
import com.google.gson.reflect.TypeToken
import com.google.gson.Gson


class SharedPreference (val context: Context) {
    private val PREFS_NAME = "bussinessTrip"
    val sharedPref: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun save(KEY_NAME: String, text: String) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, text)

        editor!!.commit()
    }

    fun save(KEY_NAME: String, value: Int) {
        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putInt(KEY_NAME, value)

        editor!!.commit()
    }

    fun save(KEY_NAME: String, status: Boolean) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putBoolean(KEY_NAME, status!!)

        editor!!.commit()
    }

    fun saveArray(KEY_NAME: String, array : ArrayList<String>){
        val editor: SharedPreferences.Editor = sharedPref.edit()
        val gson = Gson()
        val json: String = gson.toJson(array)
        editor.putString(KEY_NAME, json)
        editor!!.commit()
    }

    fun getArrayList(KEY_NAME: String): ArrayList<String> {
        val gson = Gson()
        val json = sharedPref.getString(KEY_NAME, "")

        val type = object : TypeToken<ArrayList<String>>() {
        }.type

        if (json!!.length > 0) {
            return gson.fromJson(json, type)
        }else{
            return arrayListOf()
        }
    }

    fun getValueString(KEY_NAME: String): String? {

        return sharedPref.getString(KEY_NAME, null)

    }

    fun getValueInt(KEY_NAME: String): Int {

        return sharedPref.getInt(KEY_NAME, 0)
    }

    fun getValueBoolean(KEY_NAME: String): Boolean {

        return sharedPref.getBoolean(KEY_NAME, false)

    }

    fun clearSharedPreference() {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        //sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        editor.clear()
        editor.commit()
    }

    fun removeValue(KEY_NAME: String) {

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.remove(KEY_NAME)
        editor.commit()
    }

    fun save(s: String, email: TextView?) {

    }
}